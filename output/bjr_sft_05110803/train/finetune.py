from functools import partial
from itertools import chain
import logging
import math
import os
import sys
import datasets
from argument import parser
from torchdata.datapipes.iter import IterableWrapper
import transformers
import evaluate


IGNORE_INDEX = -100




def main():
    model_args,data_args,train_args = parser()
    logging.basicConfig(
        format='[%(asctime)s %(pathname)s:%(lineno)s %(levelno)s]\t%(message)s',
        datefmt='%m/%d/%Y %H:%M:%S',
        handlers=[logging.StreamHandler(sys.stdout)]
    )
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)
    if train_args.should_log:
        transformers.utils.logging.set_verbosity_info()

    log_level = train_args.get_process_log_level()
    logger.setLevel(log_level)
    datasets.utils.logging.set_verbosity(log_level)
    transformers.utils.logging.set_verbosity(log_level)

    tokenizer = transformers.AutoTokenizer.from_pretrained(
        model_args.model_path, 
        trust_remote_code=True,
        revision = "main",
        use_fast =  model_args.use_fast_tokenizer,
        padding_side="right")
    tokenizer.pad_token = tokenizer.eos_token


    cache_path = os.path.join(data_args.data_path)
    
    with train_args.main_process_first(desc="dataset map tokenization"):
        ds = datasets.load_from_disk(cache_path)
        if train_args.do_train:
            train_dataset = ds['train']
            if data_args.max_num_data:
                train_dataset = train_dataset.select(range(data_args.max_num_data))
    model = transformers.AutoModelForCausalLM.from_pretrained(
        model_args.model_path, 
        revision = "main",
        trust_remote_code=True)
    compute_metrics = None
    preprocess_logits_for_metrics = None
    if train_args.do_eval:
        eval_dataset = ds['test']
        def preprocess_logits_for_metrics(logits, labels):
                if isinstance(logits, tuple):
                    logits = logits[0]
                return logits.argmax(dim=-1)
        metric = evaluate.load("accuracy.py")
        def compute_metrics(eval_preds):
            preds, labels = eval_preds
            # preds have the same shape as the labels, after the argmax(-1) has been calculated
            # by preprocess_logits_for_metrics but we need to shift the labels
            labels = labels[:, 1:].reshape(-1)
            preds = preds[:, :-1].reshape(-1)
            return metric.compute(predictions=preds, references=labels)
    if train_args.do_train: 
        data_collator = transformers.DataCollatorForSeq2Seq(
            tokenizer=tokenizer,
            pad_to_multiple_of=8 ,  # for shift short attention
            label_pad_token_id=IGNORE_INDEX,
            padding=True
        )   
        trainer = transformers.Trainer(
            model=model,
            args=train_args,
            train_dataset= IterableWrapper(train_dataset) if train_args.do_train else None,
            eval_dataset= IterableWrapper(eval_dataset) if train_args.do_eval else None,
            tokenizer=tokenizer,
            # Data collator will default to DataCollatorWithPadding, so we change it.
            data_collator = data_collator,
            compute_metrics = compute_metrics ,
            preprocess_logits_for_metrics = preprocess_logits_for_metrics
        )
        

    if train_args.do_train:
        logger.info(f"local_rank:{train_args.local_rank} start train")
        train_result = trainer.train()
        trainer.save_model()  # Saves the tokenizer too for easy upload

        metrics = train_result.metrics

        metrics["train_samples"] = len(train_dataset)
        trainer.log_metrics("train", metrics)
        trainer.save_metrics("train", metrics)
        trainer.save_state()


    if train_args.do_eval:
        logger.info("*** Evaluate ***")

        metrics = trainer.evaluate()
        metrics["eval_samples"] = len(eval_dataset)
        try:
            perplexity = math.exp(metrics["eval_loss"])
        except OverflowError:
            perplexity = float("inf")
        metrics["perplexity"] = perplexity
        trainer.log_metrics("eval", metrics)
        trainer.save_metrics("eval", metrics)


if __name__ == "__main__":
    main()