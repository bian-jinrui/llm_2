from functools import partial
from itertools import chain
import logging
import math
import os
import sys
import datasets
from argument import parser
from torchdata.datapipes.iter import IterableWrapper
import transformers
import evaluate
from peft import LoraConfig,TaskType,get_peft_model

IGNORE_INDEX = -100



def main():
    model_args,data_args,train_args = parser()
    logging.basicConfig(
        format='[%(asctime)s %(pathname)s:%(lineno)s %(levelno)s]\t%(message)s',
        datefmt='%m/%d/%Y %H:%M:%S',
        handlers=[logging.StreamHandler(sys.stdout)]
    )
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)
    if train_args.should_log:
        transformers.utils.logging.set_verbosity_info()

    log_level = train_args.get_process_log_level()
    logger.setLevel(log_level)
    datasets.utils.logging.set_verbosity(log_level)
    transformers.utils.logging.set_verbosity(log_level)

    tokenizer = transformers.AutoTokenizer.from_pretrained(
        model_args.model_path, 
        trust_remote_code=True,
        revision = "main",
        use_fast =  model_args.use_fast_tokenizer,
        padding_side="right")
    
    tokenizer.pad_token = tokenizer.eos_token


    cache_path = os.path.join(data_args.data_path)
    
    with train_args.main_process_first(desc="dataset map tokenization"):
        ds = datasets.load_from_disk(cache_path)
        if train_args.do_train:
            # 暂时不需要
            try:
                train_dataset = ds['train']
            except:
                train_dataset = ds
            if data_args.max_num_data:
                train_dataset = train_dataset.select(range(data_args.max_num_data))
            logger.info(f"input:{tokenizer.decode(train_dataset[0]['input_ids'])}")

    model = transformers.AutoModelForCausalLM.from_pretrained(
        model_args.model_path, 
        revision = "main",
        trust_remote_code=True)
    model.config.use_cache = False
    # lora config
        # target_modules=["q_proj", "k_proj", "down_proj"],
        # target_modules=["q_proj", "k_proj", "v_proj", "o_proj", "gate_proj", "up_proj", "down_proj"],

    config = LoraConfig(
        task_type=TaskType.CAUSAL_LM, 
        target_modules=["q_proj", "k_proj", "v_proj", "o_proj", "gate_proj", "up_proj", "down_proj"],
        inference_mode=False, # 训练模式
        r=8, # Lora 秩
        lora_alpha=32, # Lora alaph，具体作用参见 Lora 原理
        lora_dropout=0.1# Dropout 比例
    )
    if train_args.gradient_checkpointing:
        if hasattr(model, "enable_input_require_grads"):
            model.enable_input_require_grads()
        else:
            def make_inputs_require_grad(module, input, output):
                output.requires_grad_(True)
            model.get_input_embeddings().register_forward_hook(make_inputs_require_grad)
    model = get_peft_model(model, config)


    compute_metrics = None
    preprocess_logits_for_metrics = None
    if train_args.do_eval:
        eval_dataset = ds['test']
        def preprocess_logits_for_metrics(logits, labels):
                if isinstance(logits, tuple):
                    logits = logits[0]
                return logits.argmax(dim=-1)
        metric = evaluate.load("accuracy.py")
        def compute_metrics(eval_preds):
            preds, labels = eval_preds
            # preds have the same shape as the labels, after the argmax(-1) has been calculated
            # by preprocess_logits_for_metrics but we need to shift the labels
            labels = labels[:, 1:].reshape(-1)
            preds = preds[:, :-1].reshape(-1)
            return metric.compute(predictions=preds, references=labels)
    if train_args.do_train: 
        data_collator = transformers.DataCollatorForSeq2Seq(
            tokenizer=tokenizer,
            pad_to_multiple_of=8 ,  # for shift short attention
            label_pad_token_id=IGNORE_INDEX,
            padding=True
        )   
        trainer = transformers.Trainer(
            model=model,
            args=train_args,
            train_dataset= IterableWrapper(train_dataset) if train_args.do_train else None,
            eval_dataset= IterableWrapper(eval_dataset) if train_args.do_eval else None,
            tokenizer=tokenizer,
            # Data collator will default to DataCollatorWithPadding, so we change it.
            data_collator = data_collator,
            compute_metrics = compute_metrics ,
            preprocess_logits_for_metrics = preprocess_logits_for_metrics
        )
        

    if train_args.do_train:
        logger.info(f"local_rank:{train_args.local_rank} start train")
        train_result = trainer.train()
        trainer.save_model()  # Saves the tokenizer too for easy upload

        metrics = train_result.metrics

        metrics["train_samples"] = len(train_dataset)
        trainer.log_metrics("train", metrics)
        trainer.save_metrics("train", metrics)
        trainer.save_state()


    if train_args.do_eval:
        logger.info("*** Evaluate ***")

        metrics = trainer.evaluate()
        metrics["eval_samples"] = len(eval_dataset)
        try:
            perplexity = math.exp(metrics["eval_loss"])
        except OverflowError:
            perplexity = float("inf")
        metrics["perplexity"] = perplexity
        trainer.log_metrics("eval", metrics)
        trainer.save_metrics("eval", metrics)


if __name__ == "__main__":
    main()