-----------job-line-----------
[2024-05-11 08:08:33,315] [INFO] [real_accelerator.py:191:get_accelerator] Setting ds_accelerator to cuda (auto detect)
[2024-05-11 08:08:38,388] [WARNING] [runner.py:202:fetch_hostfile] Unable to find hostfile, will proceed with training with local resources only.
[2024-05-11 08:08:38,495] [INFO] [runner.py:568:main] cmd = /usr/bin/python -u -m deepspeed.launcher.launch --world_info=eyJsb2NhbGhvc3QiOiBbNiwgN119 --master_addr=127.0.0.1 --master_port=45603 --enable_each_rank_log=None /mnt/inaisfs/user-fs/test01/group2/yuyang//src/llm-train/train/finetune_lora.py --deepspeed /mnt/inaisfs/user-fs/test01/group2/yuyang//src/llm-train/train/configs/ds_config_zero3.json --model_path /mnt/inaisfs/user-fs/test01/group2/yuyang//model/Qwen1.5-0.5B-Chat --data_path /mnt/inaisfs/user-fs/test01/group2/yuyang//data/sft.cache --cache True --gradient_checkpointing True --do_train True --do_eval False --bf16 True --dispatch_batches False --learning_rate 3e-5 --per_device_train_batch_size 2 --gradient_accumulation_steps 1 --lr_scheduler_type cosine --warmup_steps 10000 --num_train_epochs 2 --save_strategy epoch --learning_rate 1e-4 --use_fast_tokenizer True --num_proc 16 --output_dir /mnt/inaisfs/user-fs/test01/group2/bianjr/src/output/bjr_sft_05110808/model --overwrite_output_dir --save_safetensor True --report_to tensorboard --logging_strategy steps --logging_steps 2 --logging_dir /mnt/inaisfs/user-fs/test01/group2/yuyang//logs/tensorboard/bjr_sft_05110808 --run_name bjr_sft_05110808
[2024-05-11 08:08:43,820] [INFO] [real_accelerator.py:191:get_accelerator] Setting ds_accelerator to cuda (auto detect)
[2024-05-11 08:08:45,144] [INFO] [launch.py:138:main] 0 NCCL_VERSION=2.19.3
[2024-05-11 08:08:45,146] [INFO] [launch.py:145:main] WORLD INFO DICT: {'localhost': [6, 7]}
[2024-05-11 08:08:45,146] [INFO] [launch.py:151:main] nnodes=1, num_local_procs=2, node_rank=0
[2024-05-11 08:08:45,146] [INFO] [launch.py:162:main] global_rank_mapping=defaultdict(<class 'list'>, {'localhost': [0, 1]})
[2024-05-11 08:08:45,146] [INFO] [launch.py:163:main] dist_world_size=2
[2024-05-11 08:08:45,146] [INFO] [launch.py:165:main] Setting CUDA_VISIBLE_DEVICES=6,7
[2024-05-11 08:08:45,148] [INFO] [launch.py:253:main] process 335747 spawned with command: ['/usr/bin/python', '-u', '/mnt/inaisfs/user-fs/test01/group2/yuyang//src/llm-train/train/finetune_lora.py', '--local_rank=0', '--deepspeed', '/mnt/inaisfs/user-fs/test01/group2/yuyang//src/llm-train/train/configs/ds_config_zero3.json', '--model_path', '/mnt/inaisfs/user-fs/test01/group2/yuyang//model/Qwen1.5-0.5B-Chat', '--data_path', '/mnt/inaisfs/user-fs/test01/group2/yuyang//data/sft.cache', '--cache', 'True', '--gradient_checkpointing', 'True', '--do_train', 'True', '--do_eval', 'False', '--bf16', 'True', '--dispatch_batches', 'False', '--learning_rate', '3e-5', '--per_device_train_batch_size', '2', '--gradient_accumulation_steps', '1', '--lr_scheduler_type', 'cosine', '--warmup_steps', '10000', '--num_train_epochs', '2', '--save_strategy', 'epoch', '--learning_rate', '1e-4', '--use_fast_tokenizer', 'True', '--num_proc', '16', '--output_dir', '/mnt/inaisfs/user-fs/test01/group2/bianjr/src/output/bjr_sft_05110808/model', '--overwrite_output_dir', '--save_safetensor', 'True', '--report_to', 'tensorboard', '--logging_strategy', 'steps', '--logging_steps', '2', '--logging_dir', '/mnt/inaisfs/user-fs/test01/group2/yuyang//logs/tensorboard/bjr_sft_05110808', '--run_name', 'bjr_sft_05110808']
[2024-05-11 08:08:45,149] [INFO] [launch.py:253:main] process 335748 spawned with command: ['/usr/bin/python', '-u', '/mnt/inaisfs/user-fs/test01/group2/yuyang//src/llm-train/train/finetune_lora.py', '--local_rank=1', '--deepspeed', '/mnt/inaisfs/user-fs/test01/group2/yuyang//src/llm-train/train/configs/ds_config_zero3.json', '--model_path', '/mnt/inaisfs/user-fs/test01/group2/yuyang//model/Qwen1.5-0.5B-Chat', '--data_path', '/mnt/inaisfs/user-fs/test01/group2/yuyang//data/sft.cache', '--cache', 'True', '--gradient_checkpointing', 'True', '--do_train', 'True', '--do_eval', 'False', '--bf16', 'True', '--dispatch_batches', 'False', '--learning_rate', '3e-5', '--per_device_train_batch_size', '2', '--gradient_accumulation_steps', '1', '--lr_scheduler_type', 'cosine', '--warmup_steps', '10000', '--num_train_epochs', '2', '--save_strategy', 'epoch', '--learning_rate', '1e-4', '--use_fast_tokenizer', 'True', '--num_proc', '16', '--output_dir', '/mnt/inaisfs/user-fs/test01/group2/bianjr/src/output/bjr_sft_05110808/model', '--overwrite_output_dir', '--save_safetensor', 'True', '--report_to', 'tensorboard', '--logging_strategy', 'steps', '--logging_steps', '2', '--logging_dir', '/mnt/inaisfs/user-fs/test01/group2/yuyang//logs/tensorboard/bjr_sft_05110808', '--run_name', 'bjr_sft_05110808']
[2024-05-11 08:08:55,688] [INFO] [real_accelerator.py:191:get_accelerator] Setting ds_accelerator to cuda (auto detect)
[2024-05-11 08:08:56,026] [INFO] [comm.py:637:init_distributed] cdb=None
[2024-05-11 08:08:56,026] [INFO] [comm.py:668:init_distributed] Initializing TorchBackend in DeepSpeed with backend nccl
[2024-05-11 08:08:58,338] [INFO] [real_accelerator.py:191:get_accelerator] Setting ds_accelerator to cuda (auto detect)
[2024-05-11 08:08:58,687] [INFO] [comm.py:637:init_distributed] cdb=None
/usr/local/lib/python3.10/dist-packages/transformers/training_args.py:1837: FutureWarning: Using `--dispatch_batches` is deprecated and will be removed in version 4.41 of 🤗 Transformers. Use `--accelerator_config {'dispatch_batches':VALUE} instead
  warnings.warn(
loading file vocab.json
loading file merges.txt
loading file tokenizer.json
loading file added_tokens.json
loading file special_tokens_map.json
loading file tokenizer_config.json
/usr/local/lib/python3.10/dist-packages/transformers/training_args.py:1837: FutureWarning: Using `--dispatch_batches` is deprecated and will be removed in version 4.41 of 🤗 Transformers. Use `--accelerator_config {'dispatch_batches':VALUE} instead
  warnings.warn(
Special tokens have been added in the vocabulary, make sure the associated word embeddings are fine-tuned or trained.
[05/11/2024 08:08:59 /mnt/inaisfs/user-fs/test01/group2/yuyang//src/llm-train/train/finetune_lora.py:57 20]	input:<|im_start|>system
You are a helpful assistant<|im_end|>
<|im_start|>user
你好<|im_end|>
<|im_start|>assistant
您好，我是小傲，一个由数智建材研究院开发的 AI 助手，很高兴认识您。请问我能为您做些什么？<|im_end|>

Special tokens have been added in the vocabulary, make sure the associated word embeddings are fine-tuned or trained.
loading configuration file /mnt/inaisfs/user-fs/test01/group2/yuyang//model/Qwen1.5-0.5B-Chat/config.json
Model config Qwen2Config {
  "_name_or_path": "/mnt/inaisfs/user-fs/test01/group2/yuyang//model/Qwen1.5-0.5B-Chat",
  "architectures": [
    "Qwen2ForCausalLM"
  ],
  "attention_dropout": 0.0,
  "bos_token_id": 151643,
  "eos_token_id": 151645,
  "hidden_act": "silu",
  "hidden_size": 1024,
  "initializer_range": 0.02,
  "intermediate_size": 2816,
  "max_position_embeddings": 32768,
  "max_window_layers": 21,
  "model_type": "qwen2",
  "num_attention_heads": 16,
  "num_hidden_layers": 24,
  "num_key_value_heads": 16,
  "rms_norm_eps": 1e-06,
  "rope_theta": 1000000.0,
  "sliding_window": 32768,
  "tie_word_embeddings": true,
  "torch_dtype": "bfloat16",
  "transformers_version": "4.40.2",
  "use_cache": true,
  "use_sliding_window": false,
  "vocab_size": 151936
}

loading weights file /mnt/inaisfs/user-fs/test01/group2/yuyang//model/Qwen1.5-0.5B-Chat/model.safetensors
Detected DeepSpeed ZeRO-3: activating zero.init() for this model
Generate config GenerationConfig {
  "bos_token_id": 151643,
  "eos_token_id": 151645
}

[2024-05-11 08:09:00,739] [INFO] [partition_parameters.py:343:__exit__] finished initializing model - num_params = 291, num_elems = 0.62B
All model checkpoint weights were used when initializing Qwen2ForCausalLM.

All the weights of Qwen2ForCausalLM were initialized from the model checkpoint at /mnt/inaisfs/user-fs/test01/group2/yuyang//model/Qwen1.5-0.5B-Chat.
If your task is similar to the task the model of the checkpoint was trained on, you can already use Qwen2ForCausalLM for predictions without further training.
loading configuration file /mnt/inaisfs/user-fs/test01/group2/yuyang//model/Qwen1.5-0.5B-Chat/generation_config.json
Generate config GenerationConfig {
  "bos_token_id": 151643,
  "do_sample": true,
  "eos_token_id": [
    151645,
    151643
  ],
  "pad_token_id": 151643,
  "repetition_penalty": 1.1,
  "top_p": 0.8
}

Using auto half precision backend
[05/11/2024 08:09:02 /mnt/inaisfs/user-fs/test01/group2/yuyang//src/llm-train/train/finetune_lora.py:123 20]	local_rank:0 start train
[2024-05-11 08:09:02,484] [INFO] [logging.py:96:log_dist] [Rank 0] DeepSpeed info: version=0.14.0, git-hash=unknown, git-branch=unknown
[2024-05-11 08:09:02,761] [INFO] [logging.py:96:log_dist] [Rank 0] DeepSpeed Flops Profiler Enabled: False
[2024-05-11 08:09:02,805] [INFO] [logging.py:96:log_dist] [Rank 0] Using client Optimizer as basic optimizer
[2024-05-11 08:09:02,806] [INFO] [logging.py:96:log_dist] [Rank 0] Removing param_group that has no 'params' in the basic Optimizer
[2024-05-11 08:09:02,884] [INFO] [logging.py:96:log_dist] [Rank 0] DeepSpeed Basic Optimizer = AdamW
[2024-05-11 08:09:02,885] [INFO] [utils.py:56:is_zero_supported_optimizer] Checking ZeRO support for optimizer=AdamW type=<class 'torch.optim.adamw.AdamW'>
[2024-05-11 08:09:02,885] [INFO] [logging.py:96:log_dist] [Rank 0] Creating fp16 ZeRO stage 3 optimizer, MiCS is enabled False, Hierarchical params gather False
[2024-05-11 08:09:02,885] [INFO] [logging.py:96:log_dist] [Rank 0] Creating torch.bfloat16 ZeRO stage 3 optimizer
[2024-05-11 08:09:03,010] [INFO] [utils.py:800:see_memory_usage] Stage 3 initialize beginning
[2024-05-11 08:09:03,012] [INFO] [utils.py:801:see_memory_usage] MA 0.63 GB         Max_MA 1.68 GB         CA 0.97 GB         Max_CA 2 GB 
[2024-05-11 08:09:03,013] [INFO] [utils.py:808:see_memory_usage] CPU Virtual Memory:  used = 65.94 GB, percent = 13.1%
[2024-05-11 08:09:03,025] [INFO] [stage3.py:130:__init__] Reduce bucket size 1048576
[2024-05-11 08:09:03,026] [INFO] [stage3.py:131:__init__] Prefetch bucket size 943718
[2024-05-11 08:09:03,140] [INFO] [utils.py:800:see_memory_usage] DeepSpeedZeRoOffload initialize [begin]
[2024-05-11 08:09:03,141] [INFO] [utils.py:801:see_memory_usage] MA 0.63 GB         Max_MA 0.63 GB         CA 0.97 GB         Max_CA 1 GB 
[2024-05-11 08:09:03,142] [INFO] [utils.py:808:see_memory_usage] CPU Virtual Memory:  used = 66.19 GB, percent = 13.1%
Parameter Offload: Total persistent parameters: 2286592 in 385 params
[2024-05-11 08:09:03,763] [INFO] [utils.py:800:see_memory_usage] DeepSpeedZeRoOffload initialize [end]
[2024-05-11 08:09:03,764] [INFO] [utils.py:801:see_memory_usage] MA 0.63 GB         Max_MA 0.63 GB         CA 0.97 GB         Max_CA 1 GB 
[2024-05-11 08:09:03,764] [INFO] [utils.py:808:see_memory_usage] CPU Virtual Memory:  used = 67.38 GB, percent = 13.4%
[2024-05-11 08:09:03,892] [INFO] [utils.py:800:see_memory_usage] Before creating fp16 partitions
[2024-05-11 08:09:03,893] [INFO] [utils.py:801:see_memory_usage] MA 0.63 GB         Max_MA 0.63 GB         CA 0.97 GB         Max_CA 1 GB 
[2024-05-11 08:09:03,893] [INFO] [utils.py:808:see_memory_usage] CPU Virtual Memory:  used = 67.66 GB, percent = 13.4%
[2024-05-11 08:09:04,175] [INFO] [utils.py:800:see_memory_usage] After creating fp16 partitions: 1
[2024-05-11 08:09:04,177] [INFO] [utils.py:801:see_memory_usage] MA 0.63 GB         Max_MA 0.63 GB         CA 0.97 GB         Max_CA 1 GB 
[2024-05-11 08:09:04,177] [INFO] [utils.py:808:see_memory_usage] CPU Virtual Memory:  used = 68.25 GB, percent = 13.6%
[2024-05-11 08:09:04,305] [INFO] [utils.py:800:see_memory_usage] Before creating fp32 partitions
[2024-05-11 08:09:04,306] [INFO] [utils.py:801:see_memory_usage] MA 0.63 GB         Max_MA 0.63 GB         CA 0.97 GB         Max_CA 1 GB 
[2024-05-11 08:09:04,306] [INFO] [utils.py:808:see_memory_usage] CPU Virtual Memory:  used = 68.48 GB, percent = 13.6%
[2024-05-11 08:09:04,432] [INFO] [utils.py:800:see_memory_usage] After creating fp32 partitions
[2024-05-11 08:09:04,434] [INFO] [utils.py:801:see_memory_usage] MA 0.63 GB         Max_MA 0.64 GB         CA 0.97 GB         Max_CA 1 GB 
[2024-05-11 08:09:04,434] [INFO] [utils.py:808:see_memory_usage] CPU Virtual Memory:  used = 68.75 GB, percent = 13.7%
[2024-05-11 08:09:04,553] [INFO] [utils.py:800:see_memory_usage] Before initializing optimizer states
[2024-05-11 08:09:04,554] [INFO] [utils.py:801:see_memory_usage] MA 0.63 GB         Max_MA 0.63 GB         CA 0.97 GB         Max_CA 1 GB 
[2024-05-11 08:09:04,554] [INFO] [utils.py:808:see_memory_usage] CPU Virtual Memory:  used = 68.99 GB, percent = 13.7%
[2024-05-11 08:09:04,668] [INFO] [utils.py:800:see_memory_usage] After initializing optimizer states
[2024-05-11 08:09:04,669] [INFO] [utils.py:801:see_memory_usage] MA 0.63 GB         Max_MA 0.64 GB         CA 0.97 GB         Max_CA 1 GB 
[2024-05-11 08:09:04,669] [INFO] [utils.py:808:see_memory_usage] CPU Virtual Memory:  used = 69.23 GB, percent = 13.7%
[2024-05-11 08:09:04,670] [INFO] [stage3.py:486:_setup_for_real_optimizer] optimizer state initialized
[2024-05-11 08:09:04,956] [INFO] [utils.py:800:see_memory_usage] After initializing ZeRO optimizer
[2024-05-11 08:09:04,958] [INFO] [utils.py:801:see_memory_usage] MA 0.64 GB         Max_MA 0.64 GB         CA 0.97 GB         Max_CA 1 GB 
[2024-05-11 08:09:04,958] [INFO] [utils.py:808:see_memory_usage] CPU Virtual Memory:  used = 69.84 GB, percent = 13.9%
[2024-05-11 08:09:04,958] [INFO] [logging.py:96:log_dist] [Rank 0] DeepSpeed Final Optimizer = AdamW
[2024-05-11 08:09:04,958] [INFO] [logging.py:96:log_dist] [Rank 0] DeepSpeed using client LR scheduler
[2024-05-11 08:09:04,958] [INFO] [logging.py:96:log_dist] [Rank 0] DeepSpeed LR Scheduler = None
[2024-05-11 08:09:04,959] [INFO] [logging.py:96:log_dist] [Rank 0] step=0, skipped=0, lr=[0.0], mom=[(0.9, 0.999)]
[2024-05-11 08:09:04,995] [INFO] [config.py:996:print] DeepSpeedEngine configuration:
[2024-05-11 08:09:04,997] [INFO] [config.py:1000:print]   activation_checkpointing_config  {
    "partition_activations": false, 
    "contiguous_memory_optimization": false, 
    "cpu_checkpointing": false, 
    "number_checkpoints": null, 
    "synchronize_checkpoint_boundary": false, 
    "profile": false
}
[2024-05-11 08:09:04,997] [INFO] [config.py:1000:print]   aio_config ................... {'block_size': 1048576, 'queue_depth': 8, 'thread_count': 1, 'single_submit': False, 'overlap_events': True}
[2024-05-11 08:09:04,997] [INFO] [config.py:1000:print]   amp_enabled .................. False
[2024-05-11 08:09:04,997] [INFO] [config.py:1000:print]   amp_params ................... False
[2024-05-11 08:09:04,997] [INFO] [config.py:1000:print]   autotuning_config ............ {
    "enabled": false, 
    "start_step": null, 
    "end_step": null, 
    "metric_path": null, 
    "arg_mappings": null, 
    "metric": "throughput", 
    "model_info": null, 
    "results_dir": "autotuning_results", 
    "exps_dir": "autotuning_exps", 
    "overwrite": true, 
    "fast": true, 
    "start_profile_step": 3, 
    "end_profile_step": 5, 
    "tuner_type": "gridsearch", 
    "tuner_early_stopping": 5, 
    "tuner_num_trials": 50, 
    "model_info_path": null, 
    "mp_size": 1, 
    "max_train_batch_size": null, 
    "min_train_batch_size": 1, 
    "max_train_micro_batch_size_per_gpu": 1.024000e+03, 
    "min_train_micro_batch_size_per_gpu": 1, 
    "num_tuning_micro_batch_sizes": 3
}
[2024-05-11 08:09:04,997] [INFO] [config.py:1000:print]   bfloat16_enabled ............. True
[2024-05-11 08:09:04,998] [INFO] [config.py:1000:print]   bfloat16_immediate_grad_update  False
[2024-05-11 08:09:04,998] [INFO] [config.py:1000:print]   checkpoint_parallel_write_pipeline  False
[2024-05-11 08:09:04,998] [INFO] [config.py:1000:print]   checkpoint_tag_validation_enabled  True
[2024-05-11 08:09:04,998] [INFO] [config.py:1000:print]   checkpoint_tag_validation_fail  False
[2024-05-11 08:09:04,998] [INFO] [config.py:1000:print]   comms_config ................. <deepspeed.comm.config.DeepSpeedCommsConfig object at 0x7f0dc1644df0>
[2024-05-11 08:09:04,998] [INFO] [config.py:1000:print]   communication_data_type ...... None
[2024-05-11 08:09:04,998] [INFO] [config.py:1000:print]   compile_config ............... enabled=False backend='inductor' kwargs={}
[2024-05-11 08:09:04,998] [INFO] [config.py:1000:print]   compression_config ........... {'weight_quantization': {'shared_parameters': {'enabled': False, 'quantizer_kernel': False, 'schedule_offset': 0, 'quantize_groups': 1, 'quantize_verbose': False, 'quantization_type': 'symmetric', 'quantize_weight_in_forward': False, 'rounding': 'nearest', 'fp16_mixed_quantize': False, 'quantize_change_ratio': 0.001}, 'different_groups': {}}, 'activation_quantization': {'shared_parameters': {'enabled': False, 'quantization_type': 'symmetric', 'range_calibration': 'dynamic', 'schedule_offset': 1000}, 'different_groups': {}}, 'sparse_pruning': {'shared_parameters': {'enabled': False, 'method': 'l1', 'schedule_offset': 1000}, 'different_groups': {}}, 'row_pruning': {'shared_parameters': {'enabled': False, 'method': 'l1', 'schedule_offset': 1000}, 'different_groups': {}}, 'head_pruning': {'shared_parameters': {'enabled': False, 'method': 'topk', 'schedule_offset': 1000}, 'different_groups': {}}, 'channel_pruning': {'shared_parameters': {'enabled': False, 'method': 'l1', 'schedule_offset': 1000}, 'different_groups': {}}, 'layer_reduction': {'enabled': False}}
[2024-05-11 08:09:04,998] [INFO] [config.py:1000:print]   curriculum_enabled_legacy .... False
[2024-05-11 08:09:04,998] [INFO] [config.py:1000:print]   curriculum_params_legacy ..... False
[2024-05-11 08:09:04,998] [INFO] [config.py:1000:print]   data_efficiency_config ....... {'enabled': False, 'seed': 1234, 'data_sampling': {'enabled': False, 'num_epochs': 1000, 'num_workers': 0, 'curriculum_learning': {'enabled': False}}, 'data_routing': {'enabled': False, 'random_ltd': {'enabled': False, 'layer_token_lr_schedule': {'enabled': False}}}}
[2024-05-11 08:09:04,998] [INFO] [config.py:1000:print]   data_efficiency_enabled ...... False
[2024-05-11 08:09:04,999] [INFO] [config.py:1000:print]   dataloader_drop_last ......... False
[2024-05-11 08:09:04,999] [INFO] [config.py:1000:print]   disable_allgather ............ False
[2024-05-11 08:09:04,999] [INFO] [config.py:1000:print]   dump_state ................... False
[2024-05-11 08:09:04,999] [INFO] [config.py:1000:print]   dynamic_loss_scale_args ...... None
[2024-05-11 08:09:04,999] [INFO] [config.py:1000:print]   eigenvalue_enabled ........... False
[2024-05-11 08:09:04,999] [INFO] [config.py:1000:print]   eigenvalue_gas_boundary_resolution  1
[2024-05-11 08:09:04,999] [INFO] [config.py:1000:print]   eigenvalue_layer_name ........ bert.encoder.layer
[2024-05-11 08:09:04,999] [INFO] [config.py:1000:print]   eigenvalue_layer_num ......... 0
[2024-05-11 08:09:04,999] [INFO] [config.py:1000:print]   eigenvalue_max_iter .......... 100
[2024-05-11 08:09:04,999] [INFO] [config.py:1000:print]   eigenvalue_stability ......... 1e-06
[2024-05-11 08:09:04,999] [INFO] [config.py:1000:print]   eigenvalue_tol ............... 0.01
[2024-05-11 08:09:04,999] [INFO] [config.py:1000:print]   eigenvalue_verbose ........... False
[2024-05-11 08:09:04,999] [INFO] [config.py:1000:print]   elasticity_enabled ........... False
[2024-05-11 08:09:05,000] [INFO] [config.py:1000:print]   flops_profiler_config ........ {
    "enabled": false, 
    "recompute_fwd_factor": 0.0, 
    "profile_step": 1, 
    "module_depth": -1, 
    "top_modules": 1, 
    "detailed": true, 
    "output_file": null
}
[2024-05-11 08:09:05,000] [INFO] [config.py:1000:print]   fp16_auto_cast ............... None
[2024-05-11 08:09:05,000] [INFO] [config.py:1000:print]   fp16_enabled ................. False
[2024-05-11 08:09:05,000] [INFO] [config.py:1000:print]   fp16_master_weights_and_gradients  False
[2024-05-11 08:09:05,000] [INFO] [config.py:1000:print]   global_rank .................. 0
[2024-05-11 08:09:05,000] [INFO] [config.py:1000:print]   grad_accum_dtype ............. None
[2024-05-11 08:09:05,000] [INFO] [config.py:1000:print]   gradient_accumulation_steps .. 1
[2024-05-11 08:09:05,000] [INFO] [config.py:1000:print]   gradient_clipping ............ 1.0
[2024-05-11 08:09:05,000] [INFO] [config.py:1000:print]   gradient_predivide_factor .... 1.0
[2024-05-11 08:09:05,000] [INFO] [config.py:1000:print]   graph_harvesting ............. False
[2024-05-11 08:09:05,000] [INFO] [config.py:1000:print]   hybrid_engine ................ enabled=False max_out_tokens=512 inference_tp_size=1 release_inference_cache=False pin_parameters=True tp_gather_partition_size=8
[2024-05-11 08:09:05,000] [INFO] [config.py:1000:print]   initial_dynamic_scale ........ 1
[2024-05-11 08:09:05,001] [INFO] [config.py:1000:print]   load_universal_checkpoint .... False
[2024-05-11 08:09:05,001] [INFO] [config.py:1000:print]   loss_scale ................... 1.0
[2024-05-11 08:09:05,001] [INFO] [config.py:1000:print]   memory_breakdown ............. False
[2024-05-11 08:09:05,001] [INFO] [config.py:1000:print]   mics_hierarchial_params_gather  False
[2024-05-11 08:09:05,001] [INFO] [config.py:1000:print]   mics_shard_size .............. -1
[2024-05-11 08:09:05,001] [INFO] [config.py:1000:print]   monitor_config ............... tensorboard=TensorBoardConfig(enabled=False, output_path='', job_name='DeepSpeedJobName') wandb=WandbConfig(enabled=False, group=None, team=None, project='deepspeed') csv_monitor=CSVConfig(enabled=False, output_path='', job_name='DeepSpeedJobName') enabled=False
[2024-05-11 08:09:05,001] [INFO] [config.py:1000:print]   nebula_config ................ {
    "enabled": false, 
    "persistent_storage_path": null, 
    "persistent_time_interval": 100, 
    "num_of_version_in_retention": 2, 
    "enable_nebula_load": true, 
    "load_path": null
}
[2024-05-11 08:09:05,001] [INFO] [config.py:1000:print]   optimizer_legacy_fusion ...... False
[2024-05-11 08:09:05,001] [INFO] [config.py:1000:print]   optimizer_name ............... None
[2024-05-11 08:09:05,001] [INFO] [config.py:1000:print]   optimizer_params ............. None
[2024-05-11 08:09:05,001] [INFO] [config.py:1000:print]   pipeline ..................... {'stages': 'auto', 'partition': 'best', 'seed_layers': False, 'activation_checkpoint_interval': 0, 'pipe_partitioned': True, 'grad_partitioned': True}
[2024-05-11 08:09:05,001] [INFO] [config.py:1000:print]   pld_enabled .................. False
[2024-05-11 08:09:05,002] [INFO] [config.py:1000:print]   pld_params ................... False
[2024-05-11 08:09:05,002] [INFO] [config.py:1000:print]   prescale_gradients ........... False
[2024-05-11 08:09:05,002] [INFO] [config.py:1000:print]   scheduler_name ............... None
[2024-05-11 08:09:05,002] [INFO] [config.py:1000:print]   scheduler_params ............. None
[2024-05-11 08:09:05,002] [INFO] [config.py:1000:print]   seq_parallel_communication_data_type  torch.float32
[2024-05-11 08:09:05,002] [INFO] [config.py:1000:print]   sparse_attention ............. None
[2024-05-11 08:09:05,002] [INFO] [config.py:1000:print]   sparse_gradients_enabled ..... False
[2024-05-11 08:09:05,002] [INFO] [config.py:1000:print]   steps_per_print .............. inf
[2024-05-11 08:09:05,002] [INFO] [config.py:1000:print]   train_batch_size ............. 4
[2024-05-11 08:09:05,002] [INFO] [config.py:1000:print]   train_micro_batch_size_per_gpu  2
[2024-05-11 08:09:05,002] [INFO] [config.py:1000:print]   use_data_before_expert_parallel_  False
[2024-05-11 08:09:05,002] [INFO] [config.py:1000:print]   use_node_local_storage ....... False
[2024-05-11 08:09:05,002] [INFO] [config.py:1000:print]   wall_clock_breakdown ......... False
[2024-05-11 08:09:05,002] [INFO] [config.py:1000:print]   weight_quantization_config ... None
[2024-05-11 08:09:05,003] [INFO] [config.py:1000:print]   world_size ................... 2
[2024-05-11 08:09:05,003] [INFO] [config.py:1000:print]   zero_allow_untested_optimizer  True
[2024-05-11 08:09:05,003] [INFO] [config.py:1000:print]   zero_config .................. stage=3 contiguous_gradients=True reduce_scatter=True reduce_bucket_size=1048576 use_multi_rank_bucket_allreduce=True allgather_partitions=True allgather_bucket_size=500,000,000 overlap_comm=True load_from_fp32_weights=True elastic_checkpoint=False offload_param=None offload_optimizer=None sub_group_size=1000000000 cpu_offload_param=None cpu_offload_use_pin_memory=None cpu_offload=None prefetch_bucket_size=943718 param_persistence_threshold=10240 model_persistence_threshold=sys.maxsize max_live_parameters=1000000000 max_reuse_distance=1000000000 gather_16bit_weights_on_model_save=True stage3_gather_fp16_weights_on_model_save=False ignore_unused_parameters=True legacy_stage1=False round_robin_gradients=False zero_hpz_partition_size=1 zero_quantized_weights=False zero_quantized_nontrainable_weights=False zero_quantized_gradients=False mics_shard_size=-1 mics_hierarchical_params_gather=False memory_efficient_linear=True pipeline_loading_checkpoint=False override_module_apply=True
[2024-05-11 08:09:05,003] [INFO] [config.py:1000:print]   zero_enabled ................. True
[2024-05-11 08:09:05,003] [INFO] [config.py:1000:print]   zero_force_ds_cpu_optimizer .. True
[2024-05-11 08:09:05,003] [INFO] [config.py:1000:print]   zero_optimization_stage ...... 3
[2024-05-11 08:09:05,003] [INFO] [config.py:986:print_user_config]   json = {
    "train_batch_size": 4, 
    "train_micro_batch_size_per_gpu": 2, 
    "gradient_accumulation_steps": 1, 
    "gradient_clipping": 1.0, 
    "zero_allow_untested_optimizer": true, 
    "fp16": {
        "enabled": false, 
        "loss_scale": 0, 
        "loss_scale_window": 1000, 
        "initial_scale_power": 16, 
        "hysteresis": 2, 
        "min_loss_scale": 1
    }, 
    "bf16": {
        "enabled": true
    }, 
    "zero_optimization": {
        "stage": 3, 
        "overlap_comm": true, 
        "contiguous_gradients": true, 
        "sub_group_size": 1.000000e+09, 
        "reduce_bucket_size": 1.048576e+06, 
        "stage3_prefetch_bucket_size": 9.437184e+05, 
        "stage3_param_persistence_threshold": 1.024000e+04, 
        "stage3_max_live_parameters": 1.000000e+09, 
        "stage3_max_reuse_distance": 1.000000e+09, 
        "stage3_gather_16bit_weights_on_model_save": true
    }, 
    "steps_per_print": inf
}
***** Running training *****
  Num examples = 158
  Num Epochs = 2
  Instantaneous batch size per device = 2
  Total train batch size (w. parallel, distributed & accumulation) = 4
  Gradient Accumulation steps = 1
  Total optimization steps = 158
  Number of trainable parameters = 3,784,704
  0%|          | 0/158 [00:00<?, ?it/s]/usr/local/lib/python3.10/dist-packages/torch/utils/checkpoint.py:434: UserWarning: torch.utils.checkpoint: please pass in use_reentrant=True or use_reentrant=False explicitly. The default value of use_reentrant will be updated to be False in the future. To maintain current behavior, pass use_reentrant=True. It is recommended that you use use_reentrant=False. Refer to docs for more details on the differences between the two variants.
  warnings.warn(
/usr/local/lib/python3.10/dist-packages/torch/utils/checkpoint.py:434: UserWarning: torch.utils.checkpoint: please pass in use_reentrant=True or use_reentrant=False explicitly. The default value of use_reentrant will be updated to be False in the future. To maintain current behavior, pass use_reentrant=True. It is recommended that you use use_reentrant=False. Refer to docs for more details on the differences between the two variants.
  warnings.warn(
  1%|          | 1/158 [00:03<08:49,  3.38s/it]