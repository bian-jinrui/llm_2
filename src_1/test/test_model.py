import os
os.environ['CUDA_VISIBLE_DEVICES']='4,5,6,7'


import torch
import transformers
import datasets
data = datasets.Dataset.from_json("compare.jsonl")

model_path = "/mnt/inaisfs/user-fs/test01/group2/yuyang/output/sft_yy_05210829/model/Qwen1.5-14B-Chatcheckpoint-40"
basename = model_path[-40:]
model = transformers.AutoModelForCausalLM.from_pretrained(model_path,trust_remote_code = True, local_files_only=True,torch_dtype = torch.bfloat16, device_map = "auto")
tokenizer = transformers.AutoTokenizer.from_pretrained(model_path)



def chat(q:str):
    messages = [
        {'role':'user','content':q}
        ]

    generate_config = {
        "repetition_penalty":1.0,
        "pad_token_id":tokenizer.eos_token_id,
        "max_new_tokens":512,
        "temperature":0.2,
        "do_sample":True
    }
    inputs = tokenizer.apply_chat_template(
        messages,
        tokenize=True,
        return_tensors="pt",
        return_dict=True,
        add_generation_prompt=True,
    ).to(model.device)
    outputs = model.generate(**inputs,**generate_config)
    outputs = outputs[0][len(inputs["input_ids"][0]):-1]
    return tokenizer.decode(outputs)


all_data = []
for i in range(len(data)):
    print(i)
    if i > 10:break
    cur_data = data[i]
    cur_output = chat(cur_data['instruction'])
    cur_data[basename] = cur_output
    all_data.append(cur_data)


import json
with open("compare.jsonl","w") as f:
    for i in all_data:
        json.dump(i,f, ensure_ascii=False)
        f.write("\n")
    f.flush()
