import json
import aiohttp
import asyncio

import glob
import random
import time
import re

query_list = ["删除基础设置中的积分制","请删除隐患排查检查","给DCS系统目录添加acc程序修改单","删除所有'日常'表单","给第一个目录添加文档表单","新增一个巡检设备管理目录，包含常见的巡检设备管理相关的表单。","在大巡检目录下新增“巡检方案”表单","生成一个水泥厂综合管理系统"]
current_time = False
system_str = '''
        你是一个语义判别助手，只需要依据要求返回0到6的数字：
        0:应用：创建，生成。
        1:表单，页面，模块，目录：添加，创建，生成
        2:表单，页面，模块，目录：不需要，删除，只需要，只保留等
        3:字段：添加，创建，生成
        4:字段：不需要，删除，只需要，只保留等
        5:可选项：添加，创建，生成
        6:其他问题
        '''
system_str = "你是一个中文的json解释助手，请你根据需求修改如下json应用表单，结构为[应用>目录>表单]：{rag_str}； \
          需求：{question}；首先请你不进行任何解释与注释，在保留原有格式的基础上完整地输出一个json表单， 然后请你先用一段中文描述这个json表单，并里面加入：‘好的，我将为你创建技术装备部综合管理系统应用，以下是应用中包含的主要功能模块，你可以按照实际需求进行内容调整，确认内容后点击“生成应用”’然后理解json中的包含关系，使用中文自然语言与其他有趣的符号代替大括号和中括号展示出来。\
          并对表单列表下的字段进行解释，在该字段后添加括号并在其中解释其在实际工作中的作用。"

system_str = """**理解提供的数据中的包含关系[应用>目录>表单]**，依据这种关系对数据中的内容使用中文自然语言与其他有趣的符号解释[📚>📂>📑]。比如下列格式:
📚消防环保设备管理系统
├── 📁设备点检巡检
│   ├── 📑设备巡检单: 记录设备的定期检查和状况评估
│   ├── 📑巡检方案: 设备检查的流程和标准
│   ├── 📑巡检方案计划: 预定的设备巡检计划安排
│   ├── 📑巡检时间计划: 每次巡检的具体时间表
├── 📁设备维护保养
│   ├── 📑设备保养单: 保养记录和维护日志
│   ├── 📑保养计划基础表: 设备保养的定期计划
├── 📁备品备件管理
│   ├── 📑备件台账: 备件库存和信息记录
│   ├── 📑备件入库单: 新增备件的记录
│   ├── 📑备件领用单: 员工或部门领用备件的记录
├── 📁设备档案/履历表: 设备详细历史记录和变更情况（无具体表单内容）
└── 📁设备报修单: 记录设备故障和维修申请（无具体表单内容）
"""

system_str = """下面的应用的结果为[应用>目录模块>表单网页]。请按照下面案例找出需要修改的**强相关**信息。
例1
问题:删除巡检目录
数据:{'巡检工作管理系统': {'大巡检': ['大巡检整改回复', '大巡检检查表'], '基础信息': ['巡检计划表', '大巡检人员统计']},'日期': ['统计日期']}
回答:['大巡检','基础信息']
例2
问题:财务管理页只保留筹资管理
数据:{'账单报销系统': {'发票': ['差旅发票', '差旅发票'], '人员信息': ['个人信息', '部门信息'],'财务管理':['筹资管理','投资管理','营运资金管理',]}}
回答:['筹资管理','投资管理']
"""


def get_app_data():
  _dir = "/mnt/inaisfs/user-fs/test01/group2/yuyang/src/llm-service_copy/prompt_data/*.json"
  prompt_list = glob.glob(_dir)
  app_json_info_path = random.choice(prompt_list)
  with open(app_json_info_path,"r") as f:
    app_json_info = f.read()
  query_str = random.choice(query_list)
  return app_json_info,query_str

def data_factory(rag_str,query_str ):
  rag_str = """{"设备管理系统": {
    "基础信息": [
      "设备信息",
      "设备型号",
      "设备厂家",
      "设备序列号",
      "设备位置",
      "设备责任人"
    ],
    "运维管理": [
      "设备运行记录",
      "设备故障记录",
      "维修保养记录",
      "设备停机报告"
    ],
    "库存管理": [
      "备件库存",
      "库存出入库记录",
      "库存预警设置",
      "库存盘点表"
    ],
    "巡检管理": [
      "巡检计划",
      "巡检任务",
      "巡检报告",
      "巡检异常处理"
    ],
    "安全管理": [
      "安全检查表",
      "设备安全评估",
      "风险点识别",
      "应急预案"
    ]
  }
}"""
  data = {
      "model": "Qwen1.5-72B-Chat",
      "messages": [
        {
            "role": "system",
            "content": system_str
        },
        {
            "role": "user",
            "content": f"问题:帮我删除风险相关的表单，数据:{rag_str}"
        }
      ]
    }
  return data


def main(max_loop:int = 1):
  global current_time
  current_time = current_time if current_time else time.strftime('%m%d%H%M%S', time.localtime())
  print(current_time)
  save_f = open(f"result_{current_time}","a")
  
  save_f.write(f"system:{system_str}\n")

  headers={"Content-Type":"application/json"}
  url = "http://10.40.0.194:12073/v1/chat/completions/"
  for idx in range(max_loop):
    async def request():
      rag_str,query_str = get_app_data()
      data = data_factory(rag_str,query_str)
      save_f.write(f"---------{idx}-----------")
      save_f.write(f'\n问题:\n{query_str}')
      # save_f.write(f'\n用户提交的表单:\n```\n{rag_str}\n```\n')
      async with aiohttp.ClientSession() as session:
        timeout = aiohttp.ClientTimeout(total=180)
        resp = await session.request("POST", url, headers=headers, json=data, timeout=timeout)
        while True:
          line = await resp.content.read(8192)
          if not line:
              break
          # trim the last newline
          # print(line.decode('utf-8',  errors='ignore').strip())
          save_f.write("\n模型回答:\n")
          generate = json.loads(line)['choices'][0]['message']['content']
          save_f.write(generate)
          match = re.findall(r'\d+', generate)
          score = int(match[0]) if match else 6
          save_f.write(f"\nscore:{score}")
          save_f.write("\n")
          save_f.flush()
    asyncio.run(request())

if __name__ == "__main__":
  main(3)