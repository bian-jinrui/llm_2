import json
import aiohttp
import asyncio

import glob
import random
import time


query_list = ["删除基础设置中的积分制","请删除隐患排查检查","给DCS系统目录添加acc程序修改单","删除所有'日常'表单","给第一个目录添加文档表单","新增一个巡检设备管理目录，包含常见的巡检设备管理相关的表单。","在大巡检目录下新增“巡检方案”表单","生成一个水泥厂综合管理系统"]
current_time = False
system_str = "reply in chinese。用```包裹返回修正的后的json数据(仅修改表单)。删除表单时没有找到相关表单或者目录,不增加未找到的目录，不胡乱删除。添加表单时添加表单时没有目录，不添加目录和表单。如果是新生成一个应用，请按照给出的格式，生成一个完整的应用表单。"
# system_str = "reply in chinese。用```包裹生成的json数据。"

def get_app_data():
  _dir = "/mnt/inaisfs/user-fs/test01/group2/yuyang/src/llm-service_copy/prompt_data/*.json"
  prompt_list = glob.glob(_dir)
  app_json_info_path = random.choice(prompt_list)
  with open(app_json_info_path,"r") as f:
    app_json_info = f.read()
  query_str = random.choice(query_list)
  return app_json_info,query_str

def data_factory(rag_str,query_str ):
  data = {
      "messages": [
        {
            "role": "system",
            "content": system_str
        },
        {
            "role": "user",
            "content": f"{query_str}。json数据:{rag_str}"
        }
      ]
    }
  return data


def main(max_loop:int = 1):
  global current_time
  current_time = current_time if current_time else time.strftime('%m%d%H%M%S', time.localtime())
  print(current_time)
  save_f = open(f"result_{current_time}","a")
  
  save_f.write(f"system:{system_str}\n")

  headers={"Content-Type":"application/json"}
  url = "http://10.0.1.61:18000/chat"
  for idx in range(max_loop):
    async def request():
      rag_str,query_str = get_app_data()
      data = data_factory(rag_str,query_str)
      save_f.write(f"---------{idx}-----------")
      save_f.write(f'\n问题:\n{query_str}')
      save_f.write(f'\n用户提交的表单:\n```\n{rag_str}\n```\n')
      async with aiohttp.ClientSession() as session:
        timeout = aiohttp.ClientTimeout(total=180)
        resp = await session.request("POST", url, headers=headers, json=data, timeout=timeout)
        while True:
          line = await resp.content.read(8192)
          if not line:
              break
          # trim the last newline
          # print(line.decode('utf-8',  errors='ignore').strip())
          save_f.write("\n模型回答:\n")
          save_f.write(json.loads(line)['generate'])
          save_f.write("\n")
          save_f.flush()
    asyncio.run(request())

if __name__ == "__main__":
  main(10)