# 导入所需的库
import json
import torch
import streamlit as st
# 在侧边栏中创建一个标题和一个链接
with st.sidebar:
    with st.container(height = 600):
        st.markdown("## 小智机器人")
        # 创建一个滑块，用于选择最大长度，范围在0到1024之间，默认值为512
        max_length = st.slider("max_length", 0, 1024, 512, step=1)
        max_new_tokens = st.slider("max_new_length",0,1024, 64 , step = 3)


# 创建一个标题和一个副标题
st.title("💬 小智对话机器人")
st.caption("⚙️ By 数智建材研究院")



# 定义一个函数，用于获取模型和tokenizer
@st.cache_resource
def get_model():
    # 定义模型路径
    import transformers

    mode_name_or_path = '/mnt/inaisfs/user-fs/test01/group2/yuyang/output/sft_yy_05210843/model/Qwen1.5-14B-Chatcheckpoint-40 '
    # 从预训练的模型中获取tokenizer
    tokenizer = transformers.AutoTokenizer.from_pretrained(mode_name_or_path, use_fast=False)
    # 从预训练的模型中获取模型，并设置模型参数
    model = transformers.AutoModelForCausalLM.from_pretrained(mode_name_or_path, torch_dtype=torch.bfloat16, device_map = "auto")
  
    return tokenizer, model

# 加载Qwen1.5-4B-Chat的model和tokenizer
tokenizer, model = get_model()

# 如果session_state中没有"messages"，则创建一个包含默认消息的列表
if "messages" not in st.session_state:
    st.session_state["messages"] = []

# 遍历session_state中的所有消息，并显示在聊天界面上
for msg in st.session_state.messages:
    st.chat_message(msg["role"]).write(msg["content"])

# 如果用户在聊天输入框中输入了内容，则执行以下操作
if prompt := st.chat_input():
    # 将用户的输入添加到session_state中的messages列表中
    st.session_state.messages.append({"role": "user", "content": prompt})
    # 在聊天界面上显示用户的输入
    st.chat_message("user").write(prompt)
    msg = st.session_state.messages
    # 构建输入     
    input_ids = tokenizer.apply_chat_template(msg,tokenize=False,add_generation_prompt=True)
    model_inputs = tokenizer([input_ids], return_tensors="pt").to(model.device)
    generated_ids = model.generate(model_inputs.input_ids, max_new_tokens=512)
    generated_ids = [
        output_ids[len(input_ids):] for input_ids, output_ids in zip(model_inputs.input_ids, generated_ids)
    ]
    response = tokenizer.batch_decode(generated_ids, skip_special_tokens=True)[0]
    # 将模型的输出添加到session_state中的messages列表中
    st.session_state.messages.append({"role": "assistant", "content": response})
    # 在聊天界面上显示模型的输出
    st.chat_message("assistant").write(response)
    print(st.session_state)


def dict_to_json(dict_data):
    return json.dumps(dict_data)

with st.sidebar:
    with st.container():
    # 将字典转换为JSON格式的字符串
        st.download_button(
            label="下载聊天记录",
            data=dict_to_json(st.session_state.messages),
            file_name='对话.json',
            mime='application/json'
        )

