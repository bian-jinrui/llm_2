
import requests
import json
import time
def get_access_token():
    """
    使用 API Key，Secret Key 获取access_token，替换下列示例中的应用API Key、应用Secret Key
    """
        
    url = "https://aip.baidubce.com/oauth/2.0/token?grant_type=client_credentials&client_id=HRuZVX678wRiKZM3OQGr8vBx&client_secret=yuzdbthAreyNo87F7sfb2nQmx4VtXBWX"
    
    payload = json.dumps("")
    headers = {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
    }
    
    response = requests.request("POST", url, headers=headers, data=payload)
    return response.json().get("access_token")

def main():
    access_token = get_access_token()

    with open("/Users/maher/Desktop/p/tools/zw6k.jsonl","r") as f:
        oridata = []
        for i in f.readlines():
            if "本文" in i: continue
            oridata.append(json.loads(i))

    # def main():
    # url = "https://aip.baidubce.com/rpc/2.0/ai_custom/v1/wenxinworkshop/chat/completions_pro?access_token=" + access_token
    url = "https://aip.baidubce.com/rpc/2.0/ai_custom/v1/wenxinworkshop/chat/completions?access_token=" + get_access_token()

    f = open("qa.jsonl","a")
    for i in oridata[138:]:
        prompt = f'请你首先理解下面的问答。根据问题，针对回答补充很多的建筑水泥行业的专业性知识，一定不能改变和删除原有的含义。问题：{i["instruction"]}\n回答：{i["output"]}'
        payload = json.dumps({
            "messages": [
                {
                    "role": "user",
                    "content": prompt
                }
            ]
        })
        headers = {
            'Content-Type': 'application/json'
        }
        try:
            response = requests.request("POST", url, headers=headers, data=payload)
            result = json.loads(response.text)
            result['prompt'] = prompt
            json.dump(result,f,ensure_ascii = False)
            f.write("\n")
            f.flush()
        except Exception as e:
            print(e)
            time.sleep(10)
            response = requests.request("POST", url, headers=headers, data=payload)
            result = json.loads(response.text)
            result['prompt'] = prompt
            json.dump(result,f,ensure_ascii = False)
            f.write("\n")
            f.flush()
        print(result['result'])
    f.close()

if __name__ == "__main__":
    
    main()