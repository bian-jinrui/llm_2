import os

import logging
import sys

import fitz

import json


from utils import clean_cnbm,tri


logging.basicConfig(
    format='[%(asctime)s %(pathname)s:%(lineno)s %(levelno)s]\t%(message)s',
    datefmt='%m/%d/%Y %H:%M:%S',
    handlers=[logging.StreamHandler(sys.stdout)]
)
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


output = "/mnt/inaisfs/user-fs/test01/group2/yuyang/data/gz_cnbm_jsonl" # dir
pdfs_path = '/mnt/inaisfs/user-fs/test01/group2/gaozh/data/中建材pdf'


def pdf_files(directory):
    for entry in os.scandir(directory):
        if entry.is_file() and entry.name.endswith('.pdf'):
            yield  entry.path,entry.name.removesuffix(".pdf")

pdfs = pdf_files(pdfs_path)

idx = 0 
for pdf,pdfname in  pdfs:
    idx += 1
    doc =  fitz.open(pdf)
    if tri(doc):
        imlist = clean_cnbm(doc)
        if len(imlist) < 2:
            logger.info(f"paser failed:{pdf}")
            continue
    else:
        logger.info(f"detect as image:{pdf}")

    with open(f"{output}/{pdfname}.jsonl","w") as f:
        for i in imlist:
            json.dump(i,f,ensure_ascii=False)
            f.write("\n")
            f.flush()
    doc.close()