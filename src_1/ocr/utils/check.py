import random

def tri(doc: any, min_len: int = 100):
    num_pages = doc.page_count 
    selected_0 = num_pages // 2
    selected_1 = random.randint(0,num_pages - 1)
    pages_num = [selected_0, selected_1 ]
    text = ""
    for page_num in pages_num:
        text = f"{text}{doc[page_num].get_text()}"
    return len(text) > min_len

