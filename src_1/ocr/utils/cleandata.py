#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
'''
@File    : cleandata.py
@Time    : 2024/05/23 11:11:39
@Author  : YuYang
@Contact : fisheepman@gmail.com
@License : Apache License Version 2.0
@Describe: https://pymupdf.readthedocs.io/en/latest/recipes-text.html
'''
import re
from typing import List
import fitz
PRE_BLOCK_NAME = "pre_block"


def flags_decomposer(flags):
    """Make font flags human readable."""
    l = []
    if flags & 2 ** 0:
        l.append("superscript")
    if flags & 2 ** 1:
        l.append("italic")
    if flags & 2 ** 2:
        l.append("serifed")
    else:
        l.append("sans")
    if flags & 2 ** 3:
        l.append("monospaced")
    else:
        l.append("proportional")
    if flags & 2 ** 4:
        l.append("bold")
    return ", ".join(l)


def remove_last_number(text):
    return re.sub(r'。\d\s+$', '。', text, flags=re.MULTILINE)

def clean_blocks(blocks,imlist:list = [], last_block_name:str|None = None):
    global PRE_BLOCK_NAME
    cur_block = None
    for b in blocks:  # iterate through the text blocks
        for l in b["lines"]:  # iterate through the text lines
            for s in l["spans"]:  # iterate through the text spans
                if (s["size"] == 18 or s["size"]  == 12) and s['font'] == 'FZHTK--GBK1-0':
                    if cur_block: 
                        if len(cur_block['内容']) < 3:
                            cur_block['标题'] += s['text']
                            cur_block['内容'] = ""
                            continue
                        cur_block['内容'] = remove_last_number(cur_block['内容'])
                        imlist.append(cur_block)
                    cur_block = {'内容':""}
                    cur_block['标题'] = s['text']
                else:
                    if not cur_block: 
                        cur_block = {'内容':""}
                        
                        if len(imlist) < 1:
                            cur_block["标题"] = PRE_BLOCK_NAME 
                        else :
                            cur_block["标题"]  = imlist[-1]['标题']
                            cur_block['内容'] = imlist[-1]['内容']
                            imlist.pop()
                    cur_block['内容'] += s['text']
    return imlist

def clean_cnbm(doc) -> List:
    """clean China National Building Materials Group pdf text
    
    ```
    import fitz

    pdf_path = "/mnt/inaisfs/user-fs/test01/group2/yuyang/data/pdf/西北地区新型相变复合保温材料的试验研究.pdf" 
    doc =  fitz.open(pdf_path)
    imlist = clean_cnbm(doc)
    ```
    """
    imlist = []
    for page in doc:
        clip_rect = fitz.Rect(page.rect.x0, page.rect.y0 + 39,page.rect.x1 ,page.rect.y1 - 70) 
        blocks = page.get_text("dict", flags=11,clip = clip_rect)["blocks"]
        imlist = clean_blocks(blocks,imlist)
    return imlist

def span_(blocks):
    for b in blocks: 
        for l in b["lines"]: 
            for s in l["spans"]: 
                yield s


def clean_cnbm_txt(doc) -> str:
    pre_page_size = None
    main_content = ""
    flag = 0
    txt = ""
    for page in doc:
        clip_rect = fitz.Rect(page.rect.x0, page.rect.y0 + 39,page.rect.x1 ,page.rect.y1 - 70) 
        blocks = page.get_text("dict", flags=11,clip = clip_rect)["blocks"]
        span_iter = span_(blocks)
        for s in span_iter:
            if not pre_page_size: pre_page_size = s['size']
            if s['size'] > 12:
                main_content = f"{main_content}{s['text']}\n"
                flag = 1
                continue
            elif s['size'] > 11:
                flag = 0
            if flag:
                main_content = f"{main_content}{s['text']}"
            elif s["size"] == pre_page_size or s["size"] < 8:
                txt = f"{txt}{s['text']}"
            else:
                txt = f"{txt}\n{s['text']}"
                pre_page_size = s["size"]
        flag = 0
    txt = f"{main_content}\n{txt}"
    return txt



def clean_txt(doc) -> str:
    pre_page_size = None
    txt = ""
    for page in doc:
        clip_rect = fitz.Rect(page.rect.x0, page.rect.y0 + 39,page.rect.x1 ,page.rect.y1 - 70) 
        blocks = page.get_text("dict", flags=11,clip = clip_rect)["blocks"]
        span_iter = span_(blocks)
        for s in span_iter:
            if not pre_page_size: pre_page_size = s['size']
            if s["size"] == pre_page_size or s["size"] < 8:
                txt = f"{txt}{s['text']}"
            else:
                txt = f"{txt}\n{s['text']}"
                pre_page_size = s["size"]
        flag = 0
    return txt