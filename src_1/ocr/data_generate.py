import requests
import json
import os
import logging
import sys


logging.basicConfig(
    format='[%(asctime)s %(pathname)s:%(lineno)s %(levelno)s]\t%(message)s',
    datefmt='%m/%d/%Y %H:%M:%S',
    handlers=[logging.StreamHandler(sys.stdout)]
)
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

def pdf_files(directory):
    for entry in os.scandir(directory):
        if entry.is_file() and entry.name.endswith('.txtl'):
            yield  entry.path
def generate(q,content,sid: int = 0):
    # sp = [ """你是一个水泥行业的专家。
    #       - 理解内容中的**数据**并解析。
    #       - 结合自身的专业知识，做**很多**详细的回答，专业的格式回答，行数尽可能多。
    #       - 回答更多内容中的数据或公式，体现出学术的专业性。
    #       - 避免回复根据本文,参考文献[1]等引用信息。""","请按照要求，返回生成问题list，要求生成的问题能够被内容所回答，\n格式:['问题0','问题1','问题2']"]

    sp = [ "你是一个水泥行业的专家，理解文章内容中的**数据**解析，以markdown格式体现公式计算(用$包裹公式)，体现出学术的专业性，结合自身的专业知识，做**很多**详细的回答，专业的格式，避免回复数据来源，回答行数尽可能多。","请按照要求，返回生成问题list，要求生成的问题能够被内容所回答，\n格式:['在氯化钠和硫酸镁溶液中浸泡28天后，PTB乳液水泥砂浆的抗蚀系数有何变化，这些变化如何影响其在实际工程中的应用?','水泥行业的趋势是什么?','在大体积混凝土温控技术措施分析中，浇筑后如何进行温度检测和养护以防止裂缝产生?']"]
    headers = {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
    data = {
            "model": "Qwen1.5-72B-Chat",
            "messages": [
                {
                    "role":"system",
                    "content": sp[sid]
                },
                {
                    "role": "user",
                    "content": f"内容:\n\n{content}"
                }
            ]
        }
    # llm_url = "http://10.40.0.194:12073/v1/chat/completions"
    llm_url = "http://10.0.1.61:18000/v1/chat/completions"

    reponse_ = requests.post(llm_url, json=data, headers=headers)
    generate = json.loads(reponse_.content)['choices'][0]['message']['content']
    return generate



if __name__ == "__main__" :
    txtls_path = "/mnt/inaisfs/user-fs/test01/group2/yuyang/data/gz_cnbm_txt"
    txtls = pdf_files(txtls_path)
    md_path = "/mnt/inaisfs/user-fs/test01/group2/yuyang/data/md_all"
    q = "请将副标题总结成不同的3个问题"
    idx_ = 0
    count_lines = "\n"
    with open("/mnt/inaisfs/user-fs/test01/group2/yuyang/data/gz_cnbm_txt_all_data.jsonl","a") as gen_f:
        for idx,txtl in enumerate(txtls):
            logger.info(f"index:{idx}   source:{txtl}")
            with open(txtl,"r") as f:
                all_content = f.read()
                if "水泥" not in all_content or "混凝土" not in all_content: continue
            for start_index in range(0,len(all_content),5000):
                content = all_content[start_index:start_index+10000]
                try:
                    text = generate(q,content,1)
                    qlist = eval(text)
                except:
                    try:
                        text = generate(q,content,1)
                        qlist = eval(text)
                    except: continue
                for qidx,q_tmp in enumerate(qlist):
                    text_tmp = generate(q_tmp,content)
                    md_f = open(f"{md_path}/{os.path.basename(txtl)}-{idx}-{qidx}.md","w")
                    md_f.write(f"{q_tmp}\n\n{text_tmp}")
                    lines_c = text_tmp.count(count_lines)
                    len_text = len(text_tmp)
                    data  = {}
                    data = {"instruction":q_tmp,"output":text_tmp,"source":txtl,"qidx":qidx,"idx":idx,"olines":lines_c,"olen":len_text}
                    json.dump(data,gen_f,ensure_ascii=False)
                    gen_f.write("\n")
                    gen_f.flush()
                    md_f.close()
                

