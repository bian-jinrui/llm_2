import os

import logging
import sys

import fitz

from utils import tri,clean_txt


logging.basicConfig(
    format='[%(asctime)s %(pathname)s:%(lineno)s %(levelno)s]\t%(message)s',
    datefmt='%m/%d/%Y %H:%M:%S',
    handlers=[logging.StreamHandler(sys.stdout)]
)
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


output = "/mnt/inaisfs/user-fs/test01/group2/yuyang/data/book_en_txt" # dir
pdfs_path = '/mnt/inaisfs/user-fs/test01/group2/yuyang/data/book_en'


def pdf_files(directory):
    for entry in os.scandir(directory):
        if entry.is_file() and entry.name.endswith('.pdf'):
            yield  entry.path,entry.name.removesuffix(".pdf")

pdfs = pdf_files(pdfs_path)

idx = 0 
for pdf, pdfname in  pdfs:
    idx += 1
    doc =  fitz.open(pdf)
    if tri(doc):
        txt = clean_txt(doc)
        if len(txt) < 3:
            logger.info(f"paser failed:{pdf}")
            continue
    else:
        logger.info(f"detect as image:{pdf}")

    with open(f"{output}/{pdfname}.txtl","w") as f:
        f.write(txt)
        f.flush()
    doc.close()