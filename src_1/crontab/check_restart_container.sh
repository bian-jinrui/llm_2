#!/bin/bash
#@File    : check_restart_container.sh
#@Time    : 2024/05/14 11:36:30
#@Author  : YuYang
#@Contact : fisheepman@gmail.com
#@License : Apache License Version 2.0
#@Description : 

for CONTAINER_NAME in "$@"; do
    # check restart container
    RUNNING=$(docker inspect -f '{{.State.Running}}' "${CONTAINER_NAME}")
    if [ "${RUNNING}" = "false" ]; then
        echo "Container ${CONTAINER_NAME} is not running. Starting it..."
        docker start "${CONTAINER_NAME}"
    fi
    sleep 2
done

# */5 * * * * /bin/bash /mnt/inaisfs/user-fs/test01/group2/yuyang/src/crontab/check_restart_container.sh serp0 serp1 >> /mnt/inaisfs/user-fs/test01/group2/yuyang/logs/crontab/check_restart_container.log 2>&1 &