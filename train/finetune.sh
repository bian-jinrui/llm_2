#!/bin/bash
export CUDA_DEVICE_MAX_CONNECTIONS=1
DIR=`pwd`

# Guide:
# This script supports distributed training on multi-gpu workers (as well as single-worker training).
# Please set the options below according to the comments.
# For multi-gpu workers training, these options should be manually set for each worker.
# After setting the options, please run the script on each worker.

# Number of GPUs per GPU worker
GPUS_PER_NODE=${GPUS_PER_NODE:2}

# Number of GPU workers, for single-worker training, please set to 1
NNODES=${NNODES:1}

# The rank of this worker, should be in {0, ..., WORKER_CNT-1}, for single-worker training, please set to 0
NODE_RANK=${NODE_RANK:0}

# The ip address of the rank-0 worker, for single-worker training, please set to localhost
MASTER_ADDR=${MASTER_ADDR:-localhost}

# The port for communication
MASTER_PORT=${MASTER_PORT:-6001}

MODEL="/mnt/inaisfs/user-fs/test01/group2/yuyang/model/Qwen1.5-0.5B-Chat" # Set the path if you do not want to load from huggingface directly
# ATTENTION: specify the path to your training data, which should be a json file consisting of a list of conversations.
# See the section for finetuning in README for more information.
DATA="/mnt/inaisfs/user-fs/test01/group2/yuyang/data/sft.cache"
DS_CONFIG_PATH="/mnt/inaisfs/user-fs/test01/group2/bianjr/src/train/ds_config_zero3.json"
USE_LORA=False
Q_LORA=False

function usage() {
    echo '
Usage: bash finetune/finetune_lora_ds.sh [-m MODEL_PATH] [-d DATA_PATH] [--deepspeed DS_CONFIG_PATH] [--use_lora USE_LORA] [--q_lora Q_LORA]
'
}



DISTRIBUTED_ARGS="
    --nproc_per_node $GPUS_PER_NODE \
"

python -m torch.distributed.run \
    --nproc_per_node 3 /mnt/inaisfs/user-fs/test01/group2/bianjr/src/train/train.py \
    --data_path "/mnt/inaisfs/user-fs/test01/group2/bianjr/src/train/qwen_data.jsonl" \
    --model_name_or_path "/mnt/inaisfs/user-fs/test01/group2/yuyang/model/Qwen1.5-0.5B-Chat" \
    --bf16 True \
    --output_dir "/mnt/inaisfs/user-fs/test01/group2/bianjr/src/output" \
    --num_train_epochs 5 \
    --per_device_train_batch_size 2 \
    --per_device_eval_batch_size 1 \
    --gradient_accumulation_steps 1 \
    --evaluation_strategy "no" \
    --save_strategy "steps" \
    --save_steps 10 \
    --save_total_limit 10 \
    --learning_rate 3e-4 \
    --weight_decay 0.01 \
    --adam_beta2 0.95 \
    --warmup_ratio 0.01 \
    --lr_scheduler_type "cosine" \
    --logging_steps 1 \
    --report_to "none" \
    --model_max_length 512 \
    --lazy_preprocess True \
    --use_lora ${USE_LORA} \
    --q_lora ${Q_LORA} \
    --gradient_checkpointing \
    --deepspeed ${DS_CONFIG_PATH}
