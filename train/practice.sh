#!/bin/bash

python -m torch.distributed.run \
    --nproc_per_node 3 /mnt/inaisfs/user-fs/test01/group2/bianjr/src/train/train.py \
    --data_path "/mnt/inaisfs/user-fs/test01/group2/yuyang/data/sft.cache" \
    --model_name_or_path "/mnt/inaisfs/user-fs/test01/group2/yuyang/model/Qwen1.5-0.5B-Chat" \
    --bf16 True \
    --output_dir "/mnt/inaisfs/user-fs/test01/group2/bianjr/src/output" \
    --num_train_epochs 5 \
    --per_device_train_batch_size 2 \
    --per_device_eval_batch_size 1 \
    --gradient_accumulation_steps 1 \
    --evaluation_strategy "no" \
    --save_strategy "steps" \
    --save_steps 10 \
    --save_total_limit 10 \
    --learning_rate 3e-4 \
    --weight_decay 0.01 \
    --adam_beta2 0.95 \
    --warmup_ratio 0.01 \
    --lr_scheduler_type "cosine" \
    --logging_steps 1 \
    --report_to "none" \
    --model_max_length 512 \
    --lazy_preprocess True \
    --use_lora ${USE_LORA} \
    --q_lora ${Q_LORA} \
    --gradient_checkpointing \
    --deepspeed ${DS_CONFIG_PATH}