from mii import pipeline
import time
from transformers import AutoTokenizer
model_path="/mnt/inaisfs/user-fs/test01/group2/yuyang/model/Qwen1.5-0.5B-Chat"
tokenizer = AutoTokenizer.from_pretrained(model_path, trust_remote_code=True, padding_side="right")

with open('/mnt/inaisfs/user-fs/test01/group2/bianjr/src/train/Contrast/longtext_new', 'r', encoding='utf-8') as file:
    # 读取所有内容并将其存储为一个字符串
    content = file.read()
print(content)
# Sample prompts.
question = "请帮我介绍一下有关水泥的相关知识"
prompts = []
for i in range(1):
    prompts.append(content)
time_list = []
token_list = []
pipe = pipeline(model_name_or_path=model_path, all_rank_output=False)
for j in range(5):
    start_time = time.time()
    output = pipe(prompts, max_new_tokens=60, temperature=0.8, top_p=0.95)
    end_time = time.time()
    last_time = end_time - start_time
    print(output)
    text = tokenizer(str(output))['input_ids']
    token_len = len(text)
    token_list.append(token_len)
    time_list.append(last_time)
print("模型推理时平均每生成一个token所用时间:" + str(sum(time_list)/sum(token_list)) + "秒")