from mii import pipeline
import mii
import time


with open('/mnt/inaisfs/user-fs/test01/group2/bianjr/src/train/Contrast/searh', 'r', encoding='utf-8') as file:
    # 读取所有内容并将其存储为一个字符串
    content = file.read()

model_path="/mnt/inaisfs/user-fs/test01/group2/yuyang/model/Qwen1.5-14B-Chat"
# Sample prompts.
question = "请帮我介绍一下有关水泥的相关知识"
prompts = []
for i in range(1):
    prompts.append(content)
time_list = []
client = mii.serve(model_path,tensor_parallel=2,replica_num=2)
for j in range(10):
    print("--------------------------------------------------------------------------------------\n")
    start_time = time.time()
    output = client.generate(prompts, max_new_tokens=100, temperature=0.8, top_p=0.95)
    end_time = time.time()
    last_time = end_time - start_time
    print(output)
    time_list.append(last_time)
    print("--------------------------------------------------------------------------------------\n")
print("模型推理的时间：" + str(sum(time_list)/10) + "秒")
client.terminate_server()