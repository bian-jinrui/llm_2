import json
import aiohttp
import asyncio

def rag_data():
   return """{
  "应用名称": "技术装备部综合管理系统",
  "目录列表":[
    {
      "type": "目录",
      "title": "日常管理",
      "表单列表":[
        "统计：文件签批",
        "日常：部门请假条",
        "子表单基础数据表",
        "部门随手捡",
        "统计：月度物资计划"
      ]
    },
    {
      "type": "目录",
      "title": "质量、环境等管理体系",
      "表单列表":[
        "电气车间受控文件清单",
        "电气车间受控记录清单"
      ]
    },
    {
      "type": "目录",
      "title": "绩效管理",
      "表单列表":[
        "绩效：行为指标申请"
      ]
    },
    {
      "type": "目录",
      "title": "精细化、安全管理",
      "表单列表":[
        "日常：整改记录",
        "整改通知单"
      ]
    },
    {
      "type": "目录",
      "title": "培训管理",
      "表单列表":[
        "培训记录",
        "培训需求申请"
      ]
    },
    {
      "type": "目录",
      "title": "项目管理",
      "表单列表":[
        "供应商清单"
      ]
    },
    {
      "type": "目录",
      "title": "档案管理",
      "表单列表":[
        "人员信息档案"
      ]
    },
    {
      "type": "目录",
      "title": "基础设置",
      "表单列表":[
        "积分制度",
        "人员配置表"
      ]
    }
  ]
}"""


rag_str = rag_data()
data = {
    "messages": [
      {
          "role": "user",
          "content": f"依据我提供的数据对其中内容以中文进行总结。提供的数据："
      }
    ]
  }

headers={"Content-Type":"application/json"}
url = "http://10.0.1.50:18000/chat"
async def request():
  async with aiohttp.ClientSession() as session:
    timeout = aiohttp.ClientTimeout(total=180)
    resp = await session.request("POST", url, headers=headers, json=data, timeout=timeout)
    while True:
      line = await resp.content.read(8192)
      if not line:
          break
      # trim the last newline
      print(line.decode('utf-8',  errors='ignore').strip())
      print(json.loads(line)['generate'])
asyncio.run(request())

