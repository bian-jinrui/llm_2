from vllm import LLM, SamplingParams
import time
from transformers import AutoTokenizer
import os
os.environ['CUDA_VISIBLE_DEVICES']='0,1,2,3,4,5,6,7'

model_path="/mnt/inaisfs/user-fs/test01/group2/yuyang/model/Qwen1.5-0.5B-Chat"
tokenizer = AutoTokenizer.from_pretrained(model_path, trust_remote_code=True, padding_side="right")

prompts = ["""在一个遥远的未来世界里，科技与自然达到了前所未有的和谐共生，人类社会经历了数次重大的变革后，建立了一套基于可持续发展和资源循环利用的新型生态系统。这个系统不仅解决了能源危机、环境污染等历史问题，还促进了星际探索与外星文明的和平交流。在这个背景下，全球政府联合体决定实施一项名为“新地平线”的计划，旨在寻找并殖民适宜人类居住的外星球，以分散地球人口压力，并进一步推动人类文明的多元化发展。
计划分为三个核心阶段：第一阶段是通过先进的太空探测器网络，对已知的类地行星进行详尽的远程分析；第二阶段是派遣载人探索队前往评估最有望适宜居住的几个候选星球，进行实地考察；第三阶段则是建设基础设施，逐步迁移部分地球居民至这些新家园。
然而，在执行第二阶段任务的过程中，探索队在一颗代号为“翡翠谷”的星球上遭遇了一系列未预料到的挑战。翡翠谷星球的环境虽然大体上与地球相似，拥有丰富的水资源和可呼吸的大气，但其独特的生物圈和地质结构给殖民工作带来了巨大困难。具体而言：
生态平衡问题：翡翠谷的生态系统中存在一种名为“光合作用兽”的本土生物，它们能够直接从大气中吸收二氧化碳并释放氧气，是该星球氧气循环的关键物种。然而，人类的初步建设活动意外破坏了光合作用兽的栖息地，导致氧气产生量骤减，对人类生存构成威胁。如何在不影响原生物种的前提下恢复并维护生态平衡？
地质稳定性：勘探发现，翡翠谷的地壳异常活跃，频繁的小型地震和不规律的板块活动对基础设施建设构成了严重威胁。科学家们预测，该星球在未来几十年内有可能发生大规模地震或火山爆发。如何利用现有技术预测并防范此类自然灾害，保障殖民地的安全？
文化融合与道德伦理：在翡翠谷的一片偏远区域，探索队意外发现了一处古老遗迹，证明了除人类之外，曾有高度发达的智慧生物在此星球上繁衍生息，但目前没有发现任何现存的外星文明迹象。这一发现引发了广泛的社会伦理讨论：人类是否有权占据并改造一个可能曾经属于其他智慧生命的星球？如何在尊重未知文明遗产的同时，合理规划人类的居住区域？
资源分配与经济体系构建：鉴于翡翠谷的资源分布不均，尤其是稀有矿产资源对于新兴殖民地的长期发展至关重要，如何设计一套公平有效的资源分配机制，确保各殖民区的均衡发展，同时避免资源争夺引发的内部冲突？
请结合以上背景和挑战，详细阐述：
如何制定一项综合性的战略规划，既能有效解决翡翠谷星球殖民过程中遇到的具体问题，又能促进人类与新环境的和谐共存，同时确保殖民活动的可持续性与道德正当性？请从生态学、地质工程、跨文化交流、资源经济学等多个角度进行全面分析，并提出具体的实施方案与步骤。"""]

with open('/mnt/inaisfs/user-fs/test01/group2/bianjr/src/train/Contrast/longtext_new', 'r', encoding='utf-8') as file:
    # 读取所有内容并将其存储为一个字符串
    content = file.read()
# Sample prompts.
prompts_1 = []
for i in range(1):
    prompts_1.append(content)
print(prompts_1)

# Create a sampling params object.
sampling_params = SamplingParams(temperature=0.8, top_p=0.95, max_tokens=1024)

# Create an LLM.
llm = LLM(model=model_path, tensor_parallel_size=8, max_model_len=4096)
# Generate texts from the prompts. The output is a list of RequestOutput objects
# that contain the prompt, generated text, and other information.
time_list = []
token_list = []

start_time = time.time()

outputs = llm.generate(prompts, sampling_params)
# Print the outputs.
end_time = time.time()
last_time = end_time - start_time
time_list.append(last_time)
print(outputs)
print("-----------------------------------------------")
for output in outputs:
    prompt = output.prompt
    generated_text = output.outputs[0].text
    text = tokenizer(generated_text)['input_ids']
    token_len = len(text)
    token_list.append(token_len)
    print(f"Prompt: {prompt!r}, Generated text: {generated_text!r}")
#print("模型推理时平均每生成一个token所用时间:" + str(sum(time_list)/sum(token_list)) + "秒")

