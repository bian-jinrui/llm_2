# llm train

需要提前熟悉deepspeed，使用offload_optimizer，得提前限制最大内存占用，导致整个机器dump。

# 训练步骤

## 清洗数据(统一数据模块)
```
data/custom/cognition/cognition.py(代码待上传)

```
## 数据格式化
```
需要将数据打包成cache
python cache.py(代码待上传)
```
## 单机多卡训练
```
bash train/sft_lora.sh
```
## 多机多卡训练（需要严格配置多机通信）
```
bash train/sft_lora_local_multi.sh
```

# 容器环境
```
docker 10.40.0.194:5000/pytorch/ds:0.3 
```

# 推理
使用vllm+langchain+实时联网爬虫
（代码待上传）