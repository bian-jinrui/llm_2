

#!/bin/bash
# -*- coding: utf-8 -*-
export CUDA_VISIBLE_DEVICES=4,5
set -e

# 回到脚本路径
cd `dirname $0`

# 避免系统过载，否则警告
export OMP_NUM_THREADS=1
# 防止新建进程报错
export TERM=xterm

export NCCL_DEBUG=INFO
export NCCL_IB_DISABLE=1
export NCCL_SOCKET_IFNAME=eth0
export NCCL_P2P_DISABLE=1
export NCCL_ASYNC_ERROR_HANDLING=1
include0="0,1"
include1="0,1"

base_dir="/mnt/inaisfs/user-fs/test01/group2/yuyang"
second_dir="/mnt/inaisfs/user-fs/test01/group2/bianjr"

type_name=sft_pt0_bjr
job_name=${type_name}_`date +%m%d%H%M`
log_dir=$second_dir/output/${job_name}
code_dir=$second_dir/src/llm-train/train



mkdir -p ${log_dir}
mkdir -p $second_dir/logs/tensorboard/$job_name
cp -r $code_dir ${log_dir}

echo code_dir:$code_dir

# my wandb
export WANDB_API_KEY=e7b2783aa6d52854b87dcb3dc6cdd01dba65c0fa
export WANDB_PROJECT=${type_name}
export WANDB_MODE=offline

# set Max_memory 300G of cpu
ulimit -d 300000000

Model_run() {
    cp $0 ${log_dir}
    cp $1 ${log_dir}
    echo "-----------job-line-----------" >> ${log_dir}/${type_name}.txt 2>&1
    # port=`python -c 'import socket; s=socket.socket(); s.bind(("", 0)); print(s.getsockname()[1]); s.close()'`
    torchrun --master_port 18510 --nproc_per_node=2 --nnodes=2 --node_rank=0  --master_addr=10.0.1.25  $1 \
        --deepspeed $code_dir/configs/ds_config_zero3.json \
        --model_path $base_dir/model/Qwen1.5-0.5B-Chat \
        --data_path $base_dir/data/sft.cache \
        --cache True \
        --gradient_checkpointing True \
        --do_train True \
        --do_eval False \
        --bf16 True \
        --dispatch_batches False \
        --learning_rate 3e-5 \
        --per_device_train_batch_size 2 \
        --gradient_accumulation_steps 1 \
        --lr_scheduler_type cosine \
        --warmup_steps 10000 \
        --num_train_epochs 2 \
        --save_strategy epoch \
        --learning_rate 1e-4 \
        --use_fast_tokenizer True \
        --num_proc 16 \
        --output_dir ${log_dir}/models \
        --overwrite_output_dir \
        --save_safetensor True \
        --report_to tensorboard \
        --logging_strategy steps \
        --logging_steps 2 \
        --logging_dir $second_dir/logs/tensorboard/$job_name \
        --run_name ${job_name} 2>&1|tee -a ${log_dir}/${type_name}.txt
    # --num_train_epochs 3 \
        # --evaluation_strategy  steps \
        # --save_steps 38419 \


    sleep 2
}


Nvidia_log() {
    echo "-----------job-line-----------" >> ${log_dir}/nvidia_log.txt
    while true
    do
        date +"%y%m%d-%H%M%S" >> ${log_dir}/nvidia_log.txt
        nvidia-smi \
        --query-gpu=index,gpu_name,memory.total,memory.used,memory.free,temperature.gpu,pstate,utilization.gpu,utilization.memory \
        --format=csv >> ${log_dir}/nvidia_log.txt
        free -h >> ${log_dir}/nvidia_log.txt
        sleep 5
    done
}



# 运行并获取pid
Model_run $code_dir/finetune_lora.py & pid_run=$!
Nvidia_log & pid_log=$!

echo Model_run pid $pid_run
echo Model_log pid $pid_log

# 等待训练主程序完成，然后关掉日志进程
wait "$pid_run"
kill "$pid_log"


sleep 2
echo "success $0"