{
    #进行gpu上面的batch size划分
    "train_batch_size": "auto",
    "train_micro_batch_size_per_gpu": "auto",
    "gradient_accumulation_steps": "auto",

    "gradient_clipping": "auto",
    #使用不支持的优化器
    "zero_allow_untested_optimizer": true,
    "fp16": {
      "enabled": "auto",
      "loss_scale": 0,
      "loss_scale_window": 1000,
      "initial_scale_power": 16,
      "hysteresis": 2,
      "min_loss_scale": 1
    },
    "bf16": {
      "enabled": "auto"
    },
    "zero_optimization": {
      "stage": 3,
      #在计算梯度的同时就进行梯度通信。overlap_comm使用的是allgather_bucket_size和reduce_bucket_size值的4.5倍。oom时可以减少
      "overlap_comm": true,
      #用于控制allreduce操作的分桶的大小，越大通信操作越快
      "reduce_bucket_size": "auto",
      #用于控制allgather操作分桶的大小，越大通信操作越快，在zero-3中不使用
      "allgather_bucket_size":"auto",
      #计算梯度时候将梯度复制到连续的缓冲区，避免后向传递过程中产生内存碎片
      "contiguous_gradients": true,
      #控制在optimizer steps中更新参数的粒度。 参数被分组到 sub_group_size 的桶中，每个桶一次更新一个。迭代器速度比较慢时候进行增大，OOM时候进行减小
      "sub_group_size": 1e9,
      #设置预取参数的固定缓冲区的大小，更小可以节省内存，但是增加通信的位置点数
      "stage3_prefetch_bucket_size": "auto",
      #小于这个阈值的时候不进行参数的划分，更小的值使用更少的内存，但是极大的增大了通信的代价
      "stage3_param_persistence_threshold": "auto",
      "stage3_max_live_parameters": 1e9,#保留在gpu上的完整参数量的上限
      "stage3_max_reuse_distance": 1e9,      #指示将来什么时候再次使用参数的指标，小于这个时间就进行保留以减少通信的开销，使用activation checkpointing时，用处明显
      #遇到oom可以减少上面两个参数，性能影响比较小（除非使用activation checkpointing）1e9消耗约2gb

      #将模型fp16权重合并以后进行模型的保存
      "stage3_gather_16bit_weights_on_model_save": true
    }
  }
'''
在使用ZERO-Infinity时候需要使用ZEro-3
为了使zero-3的速度接近zero-2,更改下面的参数:
1.将stage3_param_persistence_threshold参数设置的很大,比如6 * hidden_size * hidden_size
2.将offload_params参数关闭,可以极大改善性能


从左到右速度越来越慢,所需GPU显存越来越少
Stage 0 (DDP) < Stage 1 < Stage 2 < Stage 2 + offload < Stage 3 < Stage 3 + offloads


使用ZeRO-offload,将部分数据offload到CPU,降低对显存的需求


ZeRO-3 中未使用 allgather_partitions、allgather_bucket_size 和 reduce_scatter 配置参数


'''
