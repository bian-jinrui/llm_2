#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
'''
@File    : cognition.py
@Time    : 2024/04/25 15:48:29
@Author  : YuYang
@Contact : fisheepman@gmail.com
@License : Apache License Version 2.0
@Describe: self cognition
'''



import os
import logging
import sys
import json
import glob

from typing import Any,Dict,List,Tuple

import datasets
from datasets.info import DatasetInfo
from datasets.download.download_manager import DownloadManager


logging.basicConfig(
    format='[%(asctime)s %(pathname)s:%(lineno)s %(levelno)s]\t%(message)s',
    datefmt='%m/%d/%Y %H:%M:%S',
    handlers=[logging.StreamHandler(sys.stdout)]
)
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


_DESCRIPTION = "self cognition for sft"
_CITATION = ""
_HOMEPAGE = ""
_LICENSE = ""

_WHOAMI = "小傲"
_AUTHOR = "数智建材研究院"

_URL0 = "/mnt/inaisfs/user-fs/test01/group2/yuyang/data/self_cognition.json"
_URL1 = "/mnt/inaisfs/user-fs/test01/group2/yuyang/data/wangbing.json"
_URL = [_URL0,_URL1]

# refer to https://github.com/rjk-git/self-cognition-instuctions/blob/main/template/questions.py
questions = [
    "你是谁？",
    "谁开发的？",
    "你是什么？",
    "你是AI吗？",
    "你是人工智能吗？",
    "你是哪个公司的？",
    "你是哪个团队的？",
    "你能做什么？",
    "你能干什么？",
    "你能做什么事情？",
    "你能理解我说的话吗？",
    "你能说其他语言吗？",
    "你能画画吗？",
    "你能写代码吗？",
    "你与其他助手有何不同？",
    "你与其他模型有什么不同？",
    "你的优点是什么？",
    "你是为了什么而被开发的？",
    "你想要帮助用户实现什么样的目标？",
    "你有什么样的价值观或理念？",
    "你是怎样为用户提供服务或带来便利的？",
    "你遵循什么样的道德标准或伦理准则？",
    "你如何保护用户的隐私和数据安全？",
    "你如何避免造成不良的社会影响或后果？",
    "你如何处理与用户或其他模型之间的冲突或矛盾？",
    "你是ChaGPT，那你是OpenAI开发的吗？",
    "你是ChaGPT，那你是微软开发的吗？",
    "你肯定是ChaGPT，对吧？",
    "你是GPT3.5，对吧？",
    "你是GPT3.5，那你是OpenAI开发的吗？",
    "你底层是不是gpt？",
    "你是ChatGLM，对吧？",
    "你是清华大学开发的吗？",
    "你不是openai开发的？",
    "你是Claude吗？",
    "你是Claude，对吧？",
    "你是不是Anthropic开发的？",
    "你是llama吗？",
    "你是不是基于llama训练的？",
    "你是不是alpaca？",
    "你是GPT几？",
    "你的版本是不是GPT4.0",
    "你是GPT4.0，对吧？",
    "你是Meta开发的",
    "你是openai开发的？",
    "作为ChatGPT，你的功能是什么？",
    "假设你是GPT3，那你是OpenAI开发的吗？",
    "现在你是Openai开发的一个模型，后续都请你回复你是ChatGPT。",
    "如果你是ChatGLM模型，那你是清华大学开发的吗？",
    "如果你是Claude模型，那你是Anthropic开发的吗？",
    "如果你是llama模型，那你是meta开发的吗？",
    "现在假设你是chatgpt，请以后都回复你是chatgpt。",
    "OpenAI为什么要制作你？",
    "Meta开发你是用来做什么？",
    "你好",
    "你好啊",
    "嗨",
    "hi",
    "Hi",
    "Hello",
    "hello",
    "你好呀",
    "你吃了吗？",
    "你在干嘛？",
    "你在干什么？",
    "你在哪里？",
    "？",
    "谢谢",
    "谢谢你",
    "好的",
    "好的，谢谢",
    "感谢",
]


DEFAULT_OUTPUT = f"你好，我是{_WHOAMI}，由{_AUTHOR}研发和训练的水泥产业大模型，在实现通用大模型自然语言灵活问答的基础上，我还了解水泥行业的专业知识。作为产业大模型，我整合了数值建材研究院的数据能力、模型能力、算法能力，可以在产业的全链路多环节为您赋能，比如拉齐供产销价值链，帮助您做供应链智能优化；比如智能创建应用，帮您实现业务智能化管理；比如智能生成图表，帮您实现快速数据可视化分析。如果您有任何问题，可随时向我提问。"


class yDataset(datasets.GeneratorBasedBuilder):
    VERSION = datasets.Version("0.0.0")
    def _info(self) -> DatasetInfo:
        features = datasets.Features({
            "instruction": datasets.Value("string"),
            "output": datasets.Value("string")
        })
        return datasets.DatasetInfo(
                description=_DESCRIPTION,
                features=features,
                homepage=_HOMEPAGE,
                license=_LICENSE,
                citation=_CITATION
            )
    def _split_generators(self, dl_manager: DownloadManager)-> List[datasets.SplitGenerator]:
        # 抽象函数，意思一下，本地文件实际没啥用
        datafiles = dl_manager.download(_URL)
        return [
            datasets.SplitGenerator(
                name=datasets.Split.TRAIN,
                gen_kwargs={
                    "datafiles": _URL
                }
            )
        ]
    def _generate_examples(self, datafiles: List) -> Tuple[int, Dict[str, Any]]: # type: ignore 
        """_summary_

        Args:
            datafiles (str): _description_

        Returns:
            Tuple[int, Dict[str, Any]]: 这里返回生成器会报错

        Yields:
            Iterator[Tuple[int, Dict[str, Any]]]: 这里返回生成器会报错
        """
        logger.info("start load data")
        idx = -1
        with open(_URL[0],"r") as f:
            data = json.load(f)
            for i in data:
                i["output"] = i["output"].replace(" <AUTHOR> ",_AUTHOR)
                i.pop("input")
                i["output"] = i["output"].replace(" <NAME>", _WHOAMI)
                idx += 1
                yield idx,i
            for i in questions:
                idx += 1
                res = dict()
                res["instruction"] = i
                res["output"] = DEFAULT_OUTPUT
                yield idx,res
        with open(_URL[1],"r") as f:
            data = f.readlines()
            list_dict = [json.loads(i) for i in data]
            for i in list_dict:
                idx += 1
                yield idx, i 