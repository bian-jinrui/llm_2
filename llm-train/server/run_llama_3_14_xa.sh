#!/bin/bash
# -*- coding: utf-8 -*-

set -e

# 回到脚本路径
cd `dirname $0`

# 避免系统过载，否则警告
export OMP_NUM_THREADS=1
# 防止新建进程报错
export TERM=xterm


base_dir="/mnt/inaisfs/user-fs/test01/group2/yuyang/"

vllm_model_path="/mnt/inaisfs/user-fs/test01/group2/yuyang/model/Qwen1.5-14B-Chat-xa"

type_name=server
job_name=${type_name}_`date +%m%d%H%M`
log_dir=$base_dir/output/${job_name}
code_dir=$base_dir/src/llm-train/server



mkdir -p ${log_dir}
cp -r $code_dir ${log_dir}

echo code_dir:$code_dir

# my wandb
export WANDB_API_KEY=5728bf4cd5eb19136f52e2da3e30b7544c1bcab0
export WANDB_PROJECT=${type_name}
export WANDB_MODE=offline



Model_run() {
    cp $0 ${log_dir}
    cp $1 ${log_dir}
    echo "-----------job-line-----------" >> ${log_dir}/${type_name}.txt 2>&1
    python3 $1 $vllm_model_path 2>&1|tee -a ${log_dir}/${type_name}.txt
    sleep 2
}


Nvidia_log() {
    echo "-----------job-line-----------" >> ${log_dir}/nvidia_log.txt
    while true
    do
        date +"%y%m%d-%H%M%S" >> ${log_dir}/nvidia_log.txt
        nvidia-smi \
        --query-gpu=index,gpu_name,memory.total,memory.used,memory.free,temperature.gpu,pstate,utilization.gpu,utilization.memory \
        --format=csv >> ${log_dir}/nvidia_log.txt
        free -h >> ${log_dir}/nvidia_log.txt
        sleep 5
    done
}


# 运行并获取pid
Model_run $code_dir/app.py & pid_run=$!
Nvidia_log & pid_log=$!

echo Model_run pid $pid_run
echo Model_log pid $pid_log

# 等待训练主程序完成，然后关掉日志进程
wait "$pid_run"
kill "$pid_log"


sleep 2
echo "success $0"